## Purpose

The value files in this repo are an example of configuring the base gitlab charts at
charts.gitlab.io (https://gitlab.com/charts/charts.gitlab.io) into a setup
that can be used for the GitLab idea to production sales demo: https://about.gitlab.com/handbook/sales/demo/

## My example Setup

I reserved a new static IP in the google cloud platform, and booted a new gke cluster. Both in the same region.

### Step 1: Create your namespace

```bash
kubectl create namespace gitlab
```

### Step 2: Setup fast ssd storage for GKE

```bash
kubectl create -n gitlab -f storageclass.yaml
```

### Step 3: Deploy database for mattermost postgresql

```bash
helm install --namespace gitlab -n mattermost-db -f mattermost-db-values.yml stable/postgresql
```

This will output a server url that we will want to copy for use in the next step

### Step 4: Deploy gitlab chart

 - Update `values.yml`
   - Set `mattermost['sql_data_source']` host value to be the db url from step 1
   - Change all the calls to nip.io addresses to be your DNS/static IP setup

```bash
helm install --namespace gitlab -n bob -f values.yml gitlab/gitlab-ce
```

### Step 5: Deploy ingress controller

 - Update `ingress-controller-values.yml`
   - Update the `loadBalancerIP` to be your static IP address
   - Update the tcp 22 port to point at your gitlab service

```bash
helm install --namespace infra -n nginx-controller -f ingress-controller-values.yml stable/nginx-ingress
```

### Step 6: Deploy letsencrypt

 - Update `lego-values.yml`
  - Update the LEGO email to be your Email

```bash
helm install --namespace infra -n lego -f lego-values.yml stable/kube-lego
```

### Step 7: Deploy runner

 - Login to your new gitlab instance, grab runner registration token
 - Update `runner-values.yml`
   - Ensure gitlab url is valid in the config
   - Update the registration token with your own


### Step 8: Integrations

 - Logged in as root, you will need to setup the service templates for the Kubernetes and Prometheus integrations
